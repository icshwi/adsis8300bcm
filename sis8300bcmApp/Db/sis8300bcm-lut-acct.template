#=================================================================#
# Template file: sis8300bcm-lut-acct.template
# Database for the records specific to the LUT parameters for
# individual ACCT
# Hinko Kocevar
# November 26, 2019

record(longout, "$(P)$(R)Lut$(N)MaxPulseLength")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.LUT$(N).MAX_PULSE_LENGTH")
    field(EGU,  "us")
    field(VAL,  "0")
    field(ASG,  "critical")
}

record(longin, "$(P)$(R)Lut$(N)MaxPulseLengthR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.LUT$(N).MAX_PULSE_LENGTH")
    field(EGU,  "us")
    field(SCAN, "I/O Intr")
}

record(ao, "$(P)$(R)Lut$(N)LowerThreshold")
{
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.LUT$(N).LOWER_THRESHOLD")
    field(EGU,  "mA")
    field(PREC, "4")
    field(VAL,  "0.0")
    field(ASG,  "critical")
}

record(ai, "$(P)$(R)Lut$(N)LowerThresholdR")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.LUT$(N).LOWER_THRESHOLD")
    field(EGU,  "mA")
    field(PREC, "4")
    field(SCAN, "I/O Intr")
}

record(ao, "$(P)$(R)Lut$(N)UpperThreshold")
{
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.LUT$(N).UPPER_THRESHOLD")
    field(EGU,  "mA")
    field(PREC, "4")
    field(VAL,  "0.0")
    field(ASG,  "critical")
}

record(ai, "$(P)$(R)Lut$(N)UpperThresholdR")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.LUT$(N).UPPER_THRESHOLD")
    field(EGU,  "mA")
    field(PREC, "4")
    field(SCAN, "I/O Intr")
}

record(bo, "$(P)$(R)Lut$(N)BeamExists")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.LUT$(N).BEAM_EXISTS")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(VAL,  "0")
    field(ASG,  "critical")
}

record(bi, "$(P)$(R)Lut$(N)BeamExistsR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.LUT$(N).BEAM_EXISTS")
    field(SCAN, "I/O Intr")
    field(ZNAM, "No")
    field(ONAM, "Yes")
}

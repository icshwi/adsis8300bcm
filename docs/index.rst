ADSIS8300bcm
============

Overview
--------

...

Prerequisites
-------------

...


Functionality
-------------

...

EPICS records
-------------

Apart from the the EPICS records inherited from the ``sis8300.template`` file,
the ADSIS8300bcm driver defines additional EPICS records, specific to the BCM system
and its data channels.

Board wide EPICS records
~~~~~~~~~~~~~~~~~~~~~~~~

These records are defined in the ``sis8300bcm.template`` file. They contain board
specific records.

.. include:: sis8300bcm.rst


Data channel EPICS records
~~~~~~~~~~~~~~~~~~~~~~~~~~

These records are defined in the ``sis8300-acqcfg.template`` file. They contain per channel
specific records.

.. include:: sis8300bcm-acqcfg.rst


ACCT EPICS records
~~~~~~~~~~~~~~~~~~~~~~~~~~

These records are defined in the ``sis8300-acct.template`` file. They contain per channel
specific records.

.. include:: sis8300bcm-acct.rst


Differential channel EPICS records
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

These records are defined in the ``sis8300-diff.template`` file. They contain per 
differential channel specific records.

.. include:: sis8300bcm-diff.rst


LUT EPICS records
~~~~~~~~~~~~~~~~~

.. include:: sis8300bcm-lut-common.rst

.. include:: sis8300bcm-lut-acct.rst


.. _alarm_sets_and_types:

Alarm sets and types
~~~~~~~~~~~~~~~~~~~~

BCM system has several set of alarms which each set keeps a number of alarms which are related to a specific process.
In total we have 23 set of alarm:

* system alarms (1 set)
* ACCT alarms (10 sets)
* differential channel alarms (10 sets)
* fiber optic channel alarms (2 sets)

For each set of alarms there are three types of alarms defined:

* direct
* first
* hold

Direct (immediate) alarm shows the immediate alarm status updated at the processing clock (~ 88 MHz) rate. Direct alarm status can be also observed with the use of the probe signals.
Hold (latched) alarm latches the direct alarm and is cleared using the alarm reset PV. Hold alarms are and-gated and connected to the BCM beam permit signal sent to the FBIS system.
First alarm indicated which alarm happened first if several alarms are raised within the set.

For each alarm set all of the alarm types are exposed as EPICS PVs.

.. _trigger_types:

Trigger types
~~~~~~~~~~~~~

Three trigger types are defined:

* main trigger
* display trigger
* ACCT trigger

Main trigger is used for the trigger measurement needs and also for the differential processing. The source of main trigger should be the same as one of ACCT trigger. This trigger is sync with the stable beam and it is the trigger of after LEBT chopper.
Main trigger and ACCT trigger are different just for the BCM #1 and for the other BCM should be selected the same source. If we could ignore IScr then we could have one trigger for all ACCTs and used it as the main trigger.

Display trigger is used for capturing stream data to memory for plotting and works independent from the processing and protection functions. It means the user can set it everywhere he wants and see the waveform.

ACCT trigger is used for our processing and protection functions which are related to each ACCT. Since we have different timing and pulse width in term of position of each ACCT specially for the first BCM system so we need to have different trigger which is synchronized with the beam.
